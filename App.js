/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import * as React from 'react';
 import { View, Text, TextInput } from 'react-native';
 import{
  StyleSheet,
  StatusBar,
  Dimensions,
  TouchableOpacity
} from "react-native";

 import { NavigationContainer } from '@react-navigation/native';
 import { createStackNavigator } from '@react-navigation/stack';
 import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

 const Stack = createStackNavigator();
 const TabNavigator = createBottomTabNavigator();

import List from "./Screen/List"
import Add from "./Screen/Add"
function HomeScreen({navigation}) {
  const [variable, setVariable] =React.useState('');
  return (  
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home Screen</Text>
      <TextInput style={styles.input}
      value={variable}
      placeholder="variable"
      onChangeText={(text) => {
        setVariable(text)}
      }/>
    </View>
  );
}
export default function App() {

  return (
    <NavigationContainer>
      <Stack.Navigator >
        <Stack.Screen name="HomeScreen" component={Bottom} />
        <Stack.Screen name="List" component={HomeScreen} options={{ title: 'Home' }} />    
        <Stack.Screen name="Add" component={Add} options={{ title: 'Add' }} />                              
      </Stack.Navigator>
     </NavigationContainer>

  );
};
function Bottom(){
  return(
  <TabNavigator.Navigator tabBarOptions={{activeTintColor:"#6495ed"}}
  >      
    <TabNavigator.Screen name="List" component={List} options={{
                              tabBarLabel: 'List',
                              
                        }}  /> 
                     
    <TabNavigator.Screen name="Add" component={Add} options={{
                              tabBarLabel: 'Add',
                              
                        }}  />       
  </TabNavigator.Navigator>
  );
}
var styles =StyleSheet.create({
  buttonContainer :{
    width:150,
    height:40,
    backgroundColor:'#6495ed',
    alignItems:'center',
    justifyContent:'center',
    borderRadius:50,
    flexDirection:'row',

},
})