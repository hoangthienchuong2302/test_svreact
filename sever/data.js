const express = require('express')
const app = express();
const bodyParser= require('body-parser')
const mongoose = require('mongoose');
const { cos } = require('react-native-reanimated');
require("./model/profile")

const Profile = mongoose.model("profile")

app.use(bodyParser.json())

const mongURI = "mongodb://localhost:27017/profile"     
mongoose.connect(mongURI,{
    useNewUrlParser : true,
    useUnifiedTopology: true
})   

mongoose.connection.on("connected",() =>{
    console.log("Connect Success")
})

mongoose.connection.on("error",(err) =>{
    console.log("error",error)
})
app.post('/delete',(req,res) =>{
    Profile.findByIdAndRemove(req.body.id)
    .then(data =>{
        console.log(data)
        res.send(data)
    }).catch(err =>{
        console.log("error", err)
    })
})

app.post('/update',(req,res) =>{
    Profile.findByIdAndUpdate(req.body.id, {
        name: req.body.name,
        position: req.body.position,
    }).then(data =>{
        console.log(data)
        res.send(data)
    }).catch(err =>{
        console.log("error",err)
    })
})

app.post('/send-data',(req,res) =>{
    const profile = new Profile({
        name: req.body.name,
        position: req.body.position
    })
    profile.save()
    .then(data =>{
        console.log(data)
        res.send(data)
    }).catch(err =>{
        console.log(err)
    })
})

app.get('/',(req,res) =>{
    Profile.find({})
    .then(data =>{
        console.log(data)
        res.send(data)
    }).catch(err =>{
        console.log(err)
    })
})

app.listen(3000,() =>{
    console.log("Listening on 3000")
})