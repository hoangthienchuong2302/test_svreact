/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React,{useState} from 'react';
 import {
   SafeAreaView,
   ScrollView,
   StatusBar,
   StyleSheet,
   Text,
   useColorScheme,
   View,
   TextInput,
   Button,
   Image,
   Dimensions,
   TouchableOpacity
 } from 'react-native';
 

 
 
 
 const Add = () => {
   const isDarkMode = useColorScheme() === 'dark';
   const backgroundStyle = {
     backgroundColor: 'gray',
   };
 const [name,setName] = useState("")
 const [position,setPosition] = useState("")


   return (
     <SafeAreaView style={{width:'100%', height:'100%',backgroundColor:'wheat',alignItems: 'center', justifyContent:'center'}}>
        <View style={styles.container}>
         <TextInput 
            label='Name'
            style={styles.input}
            value={name}
            theme={theme}
            mode="outline"
            onChangeText={text => setName(text)}>
         </TextInput>
         <TextInput 
            label='Position'
            style={styles.input}
            value={position}
            theme={theme}
            mode="outline"
            onChangeText={text => setPosition(text)}>
         </TextInput>
         <TouchableOpacity style={styles.buttonContainer} 
               >
                    <Text style={styles.buttonText}>Get started</Text>
      </TouchableOpacity>
      </View>
     
     </SafeAreaView>
 
   );
 };
 const theme = {
  colors: {  
    primary: 'red',
  },
};
 const { width, height } = Dimensions.get("window");
 const styles = StyleSheet.create({

   buttonContainer :{
    width:150,
    height:40,
    backgroundColor:'#6495ed',
    alignItems:'center',
    justifyContent:'center',
    borderRadius:50,
    flexDirection:'row',

},
   container: {

   },
   input:{
    margin:6,
    borderRadius:20,
    backgroundColor:'white'
},
 });
 
 export default Add;
 