/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React from 'react';
import { useEffect } from 'react';
 import {
   SafeAreaView,
   ScrollView,
   StatusBar,
   StyleSheet,
   Text,
   useColorScheme,
   View,
   TextInput,
   Button,
   Image,
   Dimensions,
   FlatList
 } from 'react-native';
 
 import {
   Colors,
   DebugInstructions,
   Header,
   LearnMoreLinks,
   ReloadInstructions,
 } from 'react-native/Libraries/NewAppScreen';
 
 
 
 const List = ({navigation}  ) => {
   const isDarkMode = useColorScheme() === 'dark';
   const backgroundStyle = {
     backgroundColor: 'gray',
   };
 

  //  useEffect(()=>{
  //   console.log('chek log',route?.params?.otherParam)
  // },[route?.params?.otherParam])
 
   //spead rest destructoring
   const ProductItem = (props) => { 

    
    

     return (
       
       <View style={styles.listContainer}>

         <Image
           source={{ uri: props.item.image, width: 60, height: 60 }}
           style={{ borderWidth: 1, borderColor: "black" }}
         />
         <View style={styles.text} >
           <Text >
             {props.item.nameUser}
           </Text>
           <Text >
             {props.item.position}</Text>
         </View>
         <View >
           <Button
             title="Call"
             color="green"
             accessibilityLabel="Learn more about this purple button"
           />
         </View>
       </View>
     );
   };


 
 
 
 
 
   const [lists, setLists] = React.useState([
     {
       id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
       nameUser: 'Alen',
       position: 'Dev-mobile',
     },
     {
       id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
       nameUser: 'Colin',
       position: 'Alen-Manager',
     },
     {
       id: '58694a0f-3da1-471f-bd96-145571e29d72',
       nameUser: 'Chun Chalen',
       position: 'Dev-web',
     },
     {
       id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
       nameUser: 'Alen',
       position: 'Dev-mobile',
     },
     {
       id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
       nameUser: 'Colin',
       position: 'Alen-Manager',
     },
     {
       id: '58694a0f-3da1-471f-bd96-145571e29d72',
       nameUser: 'Chun Chalen',
       position: 'Dev-web',
     },
     {
       id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
       nameUser: 'Alen',
       position: 'Dev-mobile',
     },
     {
       id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
       nameUser: 'Colin',
       position: 'Alen-Manager',
     },
     {
       id: '58694a0f-3da1-471f-bd96-145571e29d72',
       nameUser: 'Chun Chalen',
       position: 'Dev-web',
     },
     {
       id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
       nameUser: 'Alen',
       position: 'Dev-mobile',
     },
     {
       id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
       nameUser: 'Colin',
       position: 'Alen-Manager',
     },
     {
       id: '58694a0f-3da1-471f-bd96-145571e29d72',
       nameUser: 'Chun Chalen',
       position: 'Dev-web',
     },]);
   return (
     <SafeAreaView style={{ flex: 1 }}>
   
         <FlatList
           keyExtractor={(item, index) => `${item.id}${index}${Date.now()}`}
           data={lists}
           renderItem={({ item, index }) => {
             return (
               <ProductItem
                 key={index}
                 item={item}
               />
             )
           }}
         />
     
     </SafeAreaView>
 
   );
 };
 const { width, height } = Dimensions.get("window");
 const styles = StyleSheet.create({
   sectionContainer: {
     marginTop: 32,
     paddingHorizontal: 24,
 
   },
   flatList: {
     width: '100%',
 
   },
   sectionTitle: {
     fontSize: 24,
     fontWeight: '600',
   },
   sectionDescription: {
     marginTop: 8,
     fontSize: 18,
     fontWeight: '400',
   },
   highlight: {
     fontWeight: '700',
   },
   listContainer: {
     backgroundColor: 'yellow',
     margin : 5,
     padding :10,
     flexDirection: "row",
     justifyContent : 'space-between',
     alignItems :'center'
   },
   container: {
     flex: 1,
     backgroundColor: 'gray'
   },
   list: {
     alignItems: 'center',
   },
   text:{
     alignItems :'center',
     flexDirection: "column",
   }
 });
 
 export default List;
 